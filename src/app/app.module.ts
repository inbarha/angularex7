import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { Routes, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';






import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    NavComponent,
    
  ],
  imports: [
    MatButtonModule,
  RouterModule.forRoot([
  
       {path:'',component:SignupComponent},
       {path:'welcome',component:WelcomeComponent},
       {path:'login',component:LoginComponent},
       {path:'**',component:SignupComponent}

      
     ]),
  
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatInputModule,
    FormsModule,
    BrowserAnimationsModule
 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
